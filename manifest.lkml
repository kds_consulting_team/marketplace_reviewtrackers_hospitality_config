project_name: "block-keboola-reviewtrackers_hospitality-config"

################ Constants ################

constant: CONFIG_PROJECT_NAME {
  value: "block-keboola-reviewtrackers_hospitality-config"
}

constant: CONNECTION {
  value: "keboola_block_reviewtrackers_hospitality"
}

constant: SCHEMA_NAME {
  value: "WORKSPACE_506436037"
}
